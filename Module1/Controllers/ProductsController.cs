﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Module1.Data;
using Module1.Models;

namespace Module1.Controllers
{
    [Route("api/Products")]
    [ApiController]
    public class ProductsController : ControllerBase
    {

        private ProductsDBContext productsDBContext;

        public ProductsController(ProductsDBContext _productsDBContext)
        {
            productsDBContext = _productsDBContext;
        }

        // GET: api/Products
        [HttpGet]
        public IEnumerable<Product> Get()
        {
            return productsDBContext.Products;
        }

        // GET: api/Products/5
        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(int id)
        {
            var product = productsDBContext.Products.SingleOrDefault(m => m.ProductID == id);
            if (product == null)
            {
                return NotFound("No Record Found...");
            }
            return Ok(product);
        }

        // POST: api/Products
        [HttpPost]
        public IActionResult Post([FromBody]Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            productsDBContext.Products.Add(product);
            productsDBContext.SaveChanges(true);
            return StatusCode(StatusCodes.Status201Created);
        }

        // PUT: api/Products/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != product.ProductID)
            {
                return BadRequest();
            }

            try
            {
                productsDBContext.Products.Update(product);
                productsDBContext.SaveChanges(true);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return NotFound("No Record Found against this id...");
            }          
            return Ok("Product updated...");
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var product = productsDBContext.Products.SingleOrDefault(m => m.ProductID == id);
            if (product == null)
            {
                return NotFound("No Record Found...");
            }
            productsDBContext.Products.Remove(product);
            productsDBContext.SaveChanges(true);
            return Ok("Product Deleted...");
        }
    }
}
