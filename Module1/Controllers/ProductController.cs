﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using Module1.Models;

namespace Module1.Controllers
{
    //[Produces("application/json")]
    [Route("api/Product1")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        static List<Product> _products = new List<Product>()
        {
            new Product(){ProductID = 0, ProductName = "Laptop", ProductPrice = "200" },
            new Product(){ProductID = 1, ProductName = "SmartPhone", ProductPrice = "100"}
        };

        /*[HttpGet]
        public IEnumerable<Product> Get()
        {
            //return _products;
        }*/

        [HttpGet]
        public IActionResult GetProduct()
        {
            //return StatusCode(StatusCode.Status201Created);
            return Ok(_products);
        }

        [HttpGet("LoadWelcomeMessage")]
        public IActionResult GetWelcomeMessage()
        {
            //return StatusCode(StatusCode.Status201Created);
            return Ok("Welcome to our store...");
        }

        [HttpPost]
        public IActionResult Post([FromBody]Product product) //product = {Product}
        {
            _products.Add(product);
            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Product product)
        {
            _products[id] = product;
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _products.RemoveAt(id);
        }
    }

}